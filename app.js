import mongoose from 'mongoose'
import express from 'express';
import dotenv from 'dotenv';
import foodRouter from './routes/foods.routes';

dotenv.config();

const app = express()
const PORT =process.env.PORT || 3000

//middleware
app.use('/api', foodRouter )

//routes
app.get('/', (req, res) =>{
    res.send("Welcome to my api")
})
mongoose.connect(process.env.MONGODB_URI)
    .then(() => console.log("connected to MongoDb Atlas"))                                        
    .catch((error) =>console.error(error))

app.listen(process.env.PORT, console.log("`Server listening on port", PORT))