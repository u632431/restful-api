import express from 'express';
import Food from '../models/foodsModel'

const foodRouter = express.Router();

// get all foods
foodRouter.get("/foods", (req, res) => {
  Food
      .find()
      .then((data) => res.json(data))
      .catch((error) => res.json({ message: error }));
  });
  
  // get a food
  foodRouter.get("/foods/:id", (req, res) => {
    const { id } = req.params;
    Food
      .findById(id)
      .then((data) => res.json(data))
      .catch((error) => res.json({ message: error }));
  });
  
  // delete a food
  foodRouter.delete("/foods/:id", (req, res) => {
    const { id } = req.params;
    Food
      .remove({ _id: id })
      .then((data) => res.json(data))
      .catch((error) => res.json({ message: error }));
  });

  export default foodRouter