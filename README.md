# restful-API
Para correr localmente, se necesita la base de datos
```
docker-compose up -d
```
* El -d, significa __detached__
* MongoDB URL Local:
```
mongodb://localhost:27017/foodb
```
# Llenar la bsase de datos con información de prueba

````
http://localhost:3000/api/food

````
# Levantar el proyecto
Modificar el archivo env.example

npm run start
