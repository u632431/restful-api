import {Schema, model } from 'mongoose';

const foodSchema = new Schema({
    id: Number,
    name: {type: String},
    description: {type: String, required: true},
    ingredients: {type: String, required: true},
    country: {type: String, required: true},
})
foodSchema.methods.toJSON = function() {
    const { estado, ...data  } = this.toObject();
    return data;
}
//crear indice de Mongo
//quiero que busques ese modelo que estoy estableciendo, sino crea el modelo en base al productSchema
const Food=  model('Food', foodSchema);

export default Food